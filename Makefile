export ARCH:=arm
export CROSS_COMPILE:=$(HOME)/arm-cross-comp-env/arm-raspbian-linux-gnueabihf/bin/arm-linux-gnueabihf-
# path du kernel utilisé au lab1 (possiblement différent pour installation custom)
# http://vision.gel.ulaval.ca/~cgagne/enseignement/SETR/H2019/labo1.html#compilation-et-installation-de-la-cha%C3%AEne-de-compilation
export KERNEL_SRC:=$(HOME)/rPi/linux-rpi-4.14.y-rt

obj-m += setr_driver_polling.o setr_driver_irq.o

# Fichier linux-rpi-4.14.y-rt/include/uapi/asm-generic/bitsperlong.h du kernel linux du labo1 ne définit pas BITS_PER_LONG (pour ARCH=arm)
KBUILD_CFLAGS += -DBITS_PER_LONG=32

all:
	printenv
	make -C $(KERNEL_SRC) M=$(PWD) modules

clean:
	make -C $(KERNEL_SRC) M=$(PWD) clean
